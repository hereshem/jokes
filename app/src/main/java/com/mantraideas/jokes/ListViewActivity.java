package com.mantraideas.jokes;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ListViewActivity extends AppCompatActivity {

    String cat_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        cat_id = getIntent().getStringExtra("cat_id");

        setTitle(getIntent().getStringExtra("name"));
        //Toast.makeText(ListViewActivity.this, "Recieved cat id = " + cat_id , Toast.LENGTH_SHORT).show();

        callServer();
    }

    private void callServer() {
        new AsyncTask<Void, Void, String>(){

            @Override
            protected String doInBackground(Void... params) {
                return new ServerRequest().httpGetData("http://jokesanjal.com/apis/jokes?cat_id="+cat_id);
            }

            @Override
            protected void onPostExecute(String s) {
                super.onPostExecute(s);
                parseData(s);
            }
        }.execute();
    }
    class Jokes{
        String image, desc, posted_by, published_date;

        public Jokes(String image, String desc, String posted_by, String published_date) {
            this.image = image;
            this.desc = desc;
            this.posted_by = posted_by;
            this.published_date = published_date;
        }
    }

    List<Jokes> jokeList = new ArrayList<>();

    private void parseData(String s) {
        try{
            JSONArray arr = new JSONArray(s);
            for (int i = 0; i < arr.length(); i++) {
                JSONObject j = arr.getJSONObject(i);
                jokeList.add(new Jokes(j.getString("image"),j.getString("description"),j.getString("posted_by"),j.getString("published_date")));
            }
            displayList();

        }catch (Exception e){e.printStackTrace();}
    }

    private void displayList() {
        ArrayAdapter adapter = new ArrayAdapter(this, R.layout.row_jokes, jokeList){
            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View v = getLayoutInflater().inflate(R.layout.row_jokes, null);

                Jokes j = jokeList.get(position);
                TextView tv = (TextView) v.findViewById(R.id.desc);
                tv.setText(j.desc);

                TextView desc = (TextView) v.findViewById(R.id.by);
                desc.setText(j.posted_by);

                TextView time = (TextView) v.findViewById(R.id.time);
                time.setText(j.published_date);

                if(j.image.length() > 0) {
                    ImageView iv = (ImageView) v.findViewById(R.id.imageView);
                    Picasso.with(getApplicationContext()).load(j.image).into(iv);
                }


                return v;
            }
        };
        ListView gv = (ListView) findViewById(R.id.listView);
        gv.setAdapter(adapter);
        gv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //Toast.makeText(ListViewActivity.this, "Clicked id = " + jokeList.get(position).desc, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(), JokesDetailActivity.class)
                        .putExtra("desc", jokeList.get(position).desc)
                        .putExtra("image", jokeList.get(position).image)
                );
            }
        });

    }
}
