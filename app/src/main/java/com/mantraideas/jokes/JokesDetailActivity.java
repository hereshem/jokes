package com.mantraideas.jokes;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class JokesDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jokes_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });


        TextView tv = (TextView) findViewById(R.id.text);

        String text = getIntent().getStringExtra("desc");
        String image = getIntent().getStringExtra("image");

        ImageView iv = (ImageView) findViewById(R.id.imageView);
        if(image.length() > 0) {

            Picasso.with(this).load(image).into(iv);
        }
        else{
            iv.setVisibility(View.GONE);
        }
        tv.setText(text);

        setTitle(text);

    }
}
